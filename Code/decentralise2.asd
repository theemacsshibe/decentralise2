(asdf:defsystem :decentralise2
  :author "Cooperative of Applied Language"
  :depends-on (:usocket :cl+ssl
               :bordeaux-threads :trivial-timeout :safe-queue
               :alexandria :cl-strings :babel
               :fset :cl-heap :trivial-backtrace
               :dynamic-mixins)
  :license "Cooperative Software License v1+"
  :version "0.1.0"
  :components ((:file "packages")
               (:module "Utilities"
                :components ((:file "thread-box")
                             (:file "defexpectation")
                             (:file "with-thread")
                             (:file "with-semaphore")
                             (:file "parse-decentralise-syntax")
                             (:file "binary-syntax")
                             (:file "never-null-let")
                             (:file "macro-magic")
                             (:file "handler-case")))
               (:module "Messages"
                :components ((:file "message-case")
                             (:file "messages")))
               (:module "Connections"
                :components ((:module "Protocol"
                              :components ((:file "class")
                                           (:file "messages")
                                           (:file "element-types")
                                           (:file "uri")))
                             (:file "pair")
                             (:file "socketed")
                             (:file "define-syntax")
                             (:file "netfarm-wire-format")))
               (:module "Systems"
                :components ((:file "protocol")
                             (:file "echo")
                             (:file "memory-database-mixin")
                             (:file "locks")
                             (:module "Standard-system"
                              :components ((:file "package")
                                           (:file "block-consistency")
                                           (:file "standard-system")
                                           (:file "special-blocks")
                                           (:file "known-blocks")
                                           (:file "requests")
                                           (:file "channels")))))
               (:module "Acceptors"
                :components ((:file "protocol")
                             (:file "socketed")))
               (:module "Client"
                :components ((:file "package")
                             (:file "conditions")
                             (:file "protocol")
                             (:file "connection-client")
                             (:file "client")
                             (:file "direct-system")))
               (:module "Kademlia"
                :components ((:file "system-mixin")
                             (:file "scheduler")
                             (:file "client")
                             (:file "search")))))
                                           
