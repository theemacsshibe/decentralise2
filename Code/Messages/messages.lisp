(in-package :decentralise-messages)

(defmacro define-message-struct (keyword name &body slots)
  (let ((constructor-name (alexandria:symbolicate "MAKE-" name))
        (slot-names (mapcar (alexandria:compose #'first #'alexandria:ensure-list)
                            slots)))
  `(progn
     (defstruct (,name (:constructor ,constructor-name ,slot-names))
       ,@slots)
     (define-message-type ,keyword ,name ,constructor-name
       ,@(loop for slot-name in slot-names
               collect (alexandria:symbolicate name "-" slot-name))))))

(define-message-struct :get get-blocks names)
(define-message-struct :block put-block
  (name (alexandria:required-argument :name) :type string)
  (version 0 :type (integer 0 *))
  (channels '() :type list)
  (data (alexandria:required-argument :data)))
(define-message-struct :ok ok-response
  (name (alexandria:required-argument :name) :type string))
(define-message-struct :error error-response
  (name (alexandria:required-argument :name) :type string)
  (reason (alexandria:required-argument :reason) :type string))
(define-message-struct :subscribe new-subscriptions
  (subscriptions '() :type list))
(define-message-struct :subscription subscription
  (name     (alexandria:required-argument :name)      :type string)
  (version  (alexandria:required-argument :version)   :type (integer 0 *))
  (channels (alexandria:required-argument :channels)  :type list))
(define-message-struct :allow-announcement announcement-control
  (allow-p (alexandria:required-argument :allow-p) :type boolean))
(define-message-struct :announce node-announcement
  (uri (alexandria:required-argument :uri) :type string)
  (id  (alexandria:required-argument :id)  :type string))
