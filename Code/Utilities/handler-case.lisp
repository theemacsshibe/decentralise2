(in-package :decentralise-utilities)

(defun parse-handlers (handlers block-name)
  (loop with no-error-form = nil
        for (name lambda-list . body) in handlers
        if (eql name :no-error)
          do (if (null no-error-form)
                 (setf no-error-form `(lambda ,lambda-list ,@body))
                 (error "multiple :NO-ERROR handlers"))
        else collect `(,name
                       (lambda ,lambda-list
                         (return-from ,block-name (progn ,@body))))
               into bindings
        finally (return (values bindings no-error-form))))

(defmacro handler-case* (form &body handlers)
  "HANDLER-CASE that tries not to ruin the stack, and thus keeps backtraces intact." 
  (alexandria:with-gensyms (block-name)
    (multiple-value-bind (bindings no-error-form)
        (parse-handlers handlers block-name)
      (if (null no-error-form)
          `(block ,block-name (handler-bind ,bindings ,form))
          `(block ,block-name
             (handler-bind ,bindings
               (multiple-value-call ,no-error-form
                                    ,form)))))))
