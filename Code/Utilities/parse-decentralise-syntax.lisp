(in-package :decentralise-utilities)

(declaim (inline render-id parse-id))
(defun render-id (id)
  (format nil "~64,'0x" id))
(defun parse-id (id-string &key (start 0) (end (length id-string)))
  (if (/= (- end start) 64)
      nil
      (handler-case
          (parse-integer id-string :radix 16 :start start :end end)
        (error () nil))))

(declaim (inline map-word-positions map-listing map-nodes))
(defun map-word-positions (function string
			   &key (delimiter #\Space)
			     (start 0) (end (length string)))
  (declare (string string))
  (loop with word-start = start
	for position from start below end
	for character = (char string position)
	do (when (char= character delimiter)
	     (funcall function word-start position)
	     (setf word-start (1+ position)))
	finally (unless (= word-start end)
		  (funcall function word-start end))))

(defun drop-spaces (stream)
  (loop for char = (peek-char nil stream nil nil)
        while (eql char #\Space)
        do (read-char stream)))

(defun read-word (stream)
  (with-output-to-string (buffer)
    (loop for char = (peek-char nil stream nil nil)
          until (member char '(nil #\Space #\Tab #\Newline))
          do (write-char char buffer)
             (read-char stream))))

(defun read-length-prefixed-string (stream)
  (let* ((length-string
           (with-output-to-string (s)
             (loop for char = (read-char stream)
                   until (char= char #\:)
                   do (write-char char s))))
         (length (parse-integer length-string))
         (string (make-string length)))
    (read-sequence string stream)
    string))

(defun write-length-prefixed-string (string stream)
  (write (length string) :stream stream :base 10)
  (write-char #\: stream)
  (write-string string stream))

(defun read-integer (stream)
  (parse-integer
   (with-output-to-string (buffer)
     (loop for char = (peek-char nil stream nil nil)
           while (and (not (null char)) (digit-char-p char))
           do (write-char (read-char stream) buffer)))))
(defun write-integer (integer stream)
  (write integer :stream stream :base 10))

(defun map-listing (function text)
  "Parse an object listing, calling FUNCTION with the arguments (NAME VERSION CHANNELS)."
  (with-input-from-string (stream text)
    (loop until (null (peek-char t stream nil nil))
          do (let ((name    (read-length-prefixed-string stream))
                   (version (progn (drop-spaces stream) (read-integer stream))))
               (loop until (progn (drop-spaces stream)
                                  (member (peek-char nil stream nil nil) '(nil #\Newline)))
                     collecting (read-length-prefixed-string stream) into channels
                     finally (funcall function name version channels))))))
(defun map-nodes (function text)
  "Parse a node listing, calling FUNCTION with the arguments (URI NODE-ID)."
  (with-input-from-string (stream text)
    (loop until (null (peek-char t stream nil nil))
          do (let ((uri     (read-length-prefixed-string stream))
                   (node-id (progn (drop-spaces stream) (read-word stream))))
               (funcall function uri node-id)))))


(defun write-listing-line (name version channels stream)
  (fresh-line stream)
  (write-length-prefixed-string name stream)
  (write-char #\Space stream)
  (write-integer version stream)
  (dolist (channel channels)
    (write-char #\Space stream)
    (write-length-prefixed-string channel stream)))

(defun write-nodes-line (uri node-id stream)
  (fresh-line stream)
  (write-length-prefixed-string uri stream)
  (write-char #\Space stream)
  (write-string (render-id node-id) stream))
