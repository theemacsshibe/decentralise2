(in-package :decentralise-client)

(defun connect-to-system (system &key (class 'connection-client))
  "Create a client that is connected directly to a system, using a passing connection."
  (multiple-value-bind (connection1 connection2)
      (decentralise-connection:make-hidden-socket)
    (decentralise-system:add-connection system connection1)
    (make-instance class :connection connection2)))
