(in-package :decentralise-standard-system)

(declaim (optimize (speed 3) (safety 1) (debug 1)))
(defpackage :decentralise-standard-system-blocks
  (:use)
  (:documentation "This package holds symbols that are used with DECENTRALISE-SYSTEM::SPECIAL-{PUT,GET} to implement inheritance and dispatch for special blocks."))

(#+sbcl sb-ext:defglobal
 #-sbcl defvar *maximum-symbol-length* 0)
(defgeneric special-put (system connection name real-name version channels data)
  (:argument-precedence-order name system connection real-name version channels data)
  (:method ((system standard-system) connection name real-name version channels data)
    (unless (decentralise-system:new-version-p system real-name version)
      (error 'too-old))
    (decentralise-system:put-block system real-name version channels data)
    (distribute-after-put-block system real-name version channels)
    (remove-old-info system name version))
  (:method :around ((system standard-system) conection name real-name version channels data)
    (with-consistent-block (real-name system)
      (call-next-method))))
(defgeneric special-get (system connection name real-name)
  (:argument-precedence-order name system connection real-name)
  (:method ((system standard-system) connection name real-name)
    (decentralise-system:get-block system real-name))
  (:method :around (system connection name real-name)
    (with-consistent-block (real-name system)
      (call-next-method))))

(defmacro define-simple-error (name &optional (string (substitute #\Space #\- (string-downcase name))))
  `(progn
     (define-condition ,name (error) ())
     (defmethod decentralise-system:describe-decentralise-error ((error ,name))
       ,string)))

(define-simple-error reserved-block)
(defun fallback (&rest rest)
  (declare (ignore rest))
  (error 'reserved-block))

(defmacro define-special-block ((class-name block-name
                                 &optional (connection-class-name 't))
                                &key get put)
  "Define a special block (a block that has some computation involved, such as listing blocks from the backend) and GET and PUT methods on it. If a method isn't provided, a fallback is used that writes the error \"reserved block\"."
  (when (symbolp block-name)
    (setf block-name (string-downcase block-name)))
  (let ((block-name (intern block-name :decentralise-standard-system-blocks))
        (name-length (length block-name)))
    `(progn
       (when (> ,name-length *maximum-symbol-length*)
         (setf *maximum-symbol-length* ,name-length))
       ,(unless (null put)
          `(defmethod special-put ((system ,class-name)
                                   (connection ,connection-class-name)
                                   (name (eql ',block-name))
                                   real-name version channels data)
             (declare (ignore real-name))
             (funcall ,put system connection version channels data)))
       ,(unless (null get)
          `(defmethod special-get ((system ,class-name)
                                   (connection ,connection-class-name)
                                   (name (eql ',block-name)) real-name)
             (declare (ignore real-name))
             (funcall ,get system connection))))))

(declaim (inline find-special-block-name))
(defun find-special-block-name (name)
  "In some implementations, FIND-SYMBOL might run quite slow with long names. This function only tries to FIND-SYMBOL if the name is short enough to be a special block name."
  (declare (string name)
           (fixnum *maximum-symbol-length*))
  (if (> (length name) *maximum-symbol-length*)
      nil
      (find-symbol name :decentralise-standard-system-blocks)))

(define-simple-error invalid-listing)
(define-simple-error too-old)
(define-special-block (standard-system "list"
                       decentralise-connection:character-connection)
  :get (lambda (system connection)
	 (decentralise-connection:with-connection-counter (n connection list)
	   (values n '()
		   (with-output-to-string (buffer)
		     (decentralise-system:map-blocks
                      (lambda (name version channels)
                        (write-listing-line name version channels buffer)) 
                      system)))))
  :put (lambda (system connection version channels data)
         (declare (ignore version channels))
	 (bt:with-lock-held ((system-known-blocks-lock system))
	   (handler-case
               (map-listing
                (lambda (name version channels)
                  (%add-known-block system connection
                                    name version channels)
                  (decentralise-connection:add-known-block connection
                                                           name
                                                           version
                                                           channels))
                data)
	     (error (e)
               (trivial-backtrace:print-backtrace e)
               (error 'invalid-listing))))))
(define-special-block (standard-system "list"
                       decentralise-connection:binary-connection)
  :get (lambda (system connection)
         (decentralise-connection:with-connection-counter (n connection list)
           (values n '()
                   (with-output-to-byte-array (consumer)
                     (decentralise-system:map-blocks
                      (lambda (name version channels)
                        (write-listing-data name version channels #'consumer))
                      system)))))
  :put (lambda (system connection version channels data)
         (declare (ignore version channels))
	 (bt:with-lock-held ((system-known-blocks-lock system))
           (handler-case
               (map-binary-listing (lambda (name version channels)
                                     (%add-known-block system connection
                                                       name version channels))
                                   data)
	     (error (e)
               (trivial-backtrace:print-backtrace e)
               (error 'invalid-listing))))))
           
(define-simple-error invalid-id)
(define-special-block (standard-system "id")
  :get (lambda (system connection)
         (declare (ignore connection))
         (values 0 '() (format nil "~64,'0x"
                               (decentralise-system:system-id system))))
  :put (lambda (system connection version channels data)
         (declare (ignore system version channels))
         (setf (decentralise-connection:connection-id connection)
               (or (parse-id data)
                   (error 'invalid-id)))))

(define-special-block (standard-system "nodes")
  :get (lambda (system this-connection)
	 (decentralise-connection:with-connection-counter (n this-connection nodes)
	   (values n '()
		   (with-output-to-string (buffer)
		     (with-unlocked-box (connections
                                         (decentralise-system:system-connections system))
		       (loop for connection in connections
			     unless (or (not (decentralise-connection:connection-announcing-p connection))
					(null (decentralise-connection:connection-id connection))
					(eq connection this-connection))
			       do (write-nodes-line
                                   (decentralise-connection:connection-uri connection)
                                   (decentralise-connection:connection-id connection)
                                   buffer)))))))
  ;; TODO: This is useful for peer discovery, which we don't implement yet.
  ;; Should we? Should this be another mixin?
  :put (lambda (system connection version channels data)
         (declare (ignore system connection version channels))
         (handler-case (map-nodes (constantly nil) data)
           (error () (error 'invalid-listing))
           (:no-error (nodes)
             (declare (ignore nodes))
             '|I caren't|))))
