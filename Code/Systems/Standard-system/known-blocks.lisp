(in-package :decentralise-standard-system)

;;; Block metadata is stored in two tables, and request generation uses a
;;; queue of block names.
;;; Metadata for interesting objects is stored in the "interesting block table",
;;; and for uninteresting objects is stored in the "uninteresting block table",
;;; which are fairly fitting names.

;;; The "interesting block queue" is a queue with names of blocks that are very
;;; likely to be requestable from a connection. Names are enqueued by
;;; %ADD-KNOWN-BLOCK and (successful or unsuccessful) completion of requests,
;;; and GET-NEXT-REQUEST-NAME will dequeue names.
;;; The queue used to be "circular", in that %ADD-KNOWN-BLOCK would put back
;;; block names that couldn't be requested, but that caused weird things to
;;; happen with locks, and the system would get jammed.

(declaim (optimize (speed 3) (safety 1) (debug 1)))
(defstruct block-info connection version channels)

(defmacro with-block-info ((connection version channels) info &body body)
  (alexandria:once-only (info)
    `(let ((,connection (block-info-connection ,info))
           (,version    (block-info-version    ,info))
           (,channels   (block-info-channels   ,info)))
       ,@body)))

(defgeneric add-known-blocks (system connection blocks)
  (:documentation "Add information about blocks we heard about to the system.
This should be used for multiple blocks instead of repeatedly calling ADD-KNOWN-BLOCK, because it only locks the system once..")
  (:method ((system standard-system) connection blocks)
    (bt:with-lock-held ((system-known-blocks-lock system))
      (map 'nil
           (lambda (block)
             (apply #'%add-known-block system connection block))
           blocks))))

(defgeneric add-known-block (system connection name version channels)
  (:documentation "Add information about a block we heard about to the system.")
  (:method ((system standard-system) connection name version channels)
    (bt:with-lock-held ((system-known-blocks-lock system))
      (%add-known-block system connection name version channels))))

(declaim (inline sethash-unless-null partition))
(defun sethash-unless-null (key hash-table value)
  "Usually sets a value in a hash-table, but removes the key if the value is null."
  (if (null value)
      (remhash key hash-table)
      (setf (gethash key hash-table) value)))

(defun partition (predicate list)
  "Return a list of every item in LIST that satisfies PREDICATE, and a list of every item that does not."
  (declare (function predicate))
  (loop for item in list
        if (funcall predicate item)
          collect item into truthy
        else
          collect item into falsy
        finally (return (values truthy falsy))))

;;; Updating a system for a changed interesting object predicate
(defgeneric move-information-between-tables
    (system name from-table to-table interesting?)
  (:method ((system standard-system) name from-table to-table interesting?)
    (multiple-value-bind (move keep)
        (partition (lambda (info)
                     (with-block-info (connection version channels)
                         info
                       (declare (ignore connection))
                       (eql (interesting-block-p system name
                                                 version channels)
                            interesting?)))
                   (gethash name from-table))
      (sethash-unless-null name from-table keep)
      (let ((to-contents (union (gethash name to-table)
                                move
                                :test #'equal)))
        ;; Add the now interesting block to the interesting block queue  
        (when interesting?
          (safe-queue:enqueue name (system-interesting-blocks-queue system)))
        (sethash-unless-null name to-table to-contents)))))

(defgeneric move-information-by-set-designator
    (system set-designator from-table to-table interesting?)
  (:method ((system standard-system) (all (eql t))
            from-table to-table interesting?)
    "Check every known block." 
    (loop for name being the hash-keys of from-table
          do (move-information-between-tables system name
                                              from-table to-table interesting?)))
  (:method ((system standard-system) (set list)
            from-table to-table interesting?)
    "Check every known block in the list SET."
    (dolist (name set)
      (when (nth-value 1 (gethash name from-table))
        (move-information-between-tables system name
                                         from-table to-table interesting?))))
  (:method ((system standard-system) (predicate function)
            from-table to-table interesting?)
    "Check every known block whose name satisfies PREDICATE."
    (loop for name being the hash-keys of from-table
          when (funcall predicate name)
            do (move-information-between-tables system name
                                                from-table to-table
                                                interesting?))))

(defgeneric update-system-for-new-interesting-block-predicate
    (system interesting-set uninteresting-set)
  (:documentation "Update bookkeeping in SYSTEM to accomodate changes in INTERESTING-OBJECT-P.
Both INTERESTING-SET and UNINTERESTING-SET can be a list of changed block statuses, or T if all known blocks should be reconsidered.
This is likely one of the longest symbols you will ever type.")
  (:method ((system standard-system) interesting-set uninteresting-set)
    (bt:with-lock-held ((system-known-blocks-lock system))
      (let ((interesting (system-interesting-blocks system))
            (uninteresting (system-uninteresting-blocks system)))
        (move-information-by-set-designator system interesting-set
                                            uninteresting interesting
                                            t)
        (move-information-by-set-designator system uninteresting-set
                                            interesting uninteresting
                                            nil)))))

(defgeneric %add-known-block (system connection name version channels)
  (:documentation "The implementation of ADD-KNOWN-BLOCK and ADD-KNOWN-BLOCKS, which assumes we have the known-blocks-lock.")
  (:method ((system standard-system) connection name version channels)
    (let* ((interesting? (interesting-block-p system name version channels))
           (table (if interesting?
                      (system-interesting-blocks system)
                      (system-uninteresting-blocks system))))
      (when interesting?
        (safe-queue:enqueue name (system-interesting-blocks-queue system)))
      (setf (gethash name table)
            (merge 'list
                   (list (make-block-info :connection connection
                                          :version version
                                          :channels channels))
                   (remove connection (gethash name table)
                           :key #'block-info-connection)
                   #'< :key #'block-info-version)))))

(defgeneric remove-old-info (system name maximum-version)
  (:documentation "Remove information about any versions of a block named NAME not newer than MAXIMUM-VERSION, which we don't need.")
  (:method ((system standard-system) name maximum-version)
    (bt:with-lock-held ((system-known-blocks-lock system))
      (dolistit ((table
                  (system-interesting-blocks system)
                  (system-uninteresting-blocks system)))
        (sethash-unless-null name table
                             (remove-if (lambda (version)
                                          (<= version maximum-version))
                                        (gethash name table)
                                        :key #'block-info-version)))
      (when (gethash name (system-interesting-blocks system))
        (safe-queue:enqueue name (system-interesting-blocks-queue system))))))

(defgeneric remove-connection-info (system connection)
  (:documentation "Remove all known blocks from the now disconnected connection.")
  (:method ((system standard-system) connection)
    (bt:with-lock-held ((system-known-blocks-lock system))
      (dolistit ((table
                  (system-interesting-blocks system)
                  (system-uninteresting-blocks system)))
        (maphash (lambda (name values)
                   (sethash-unless-null name table
                                        (remove connection values
                                                :key #'block-info-connection)))
                 table)))))

(defgeneric get-connection-for-name (system name)
  (:documentation "Get a connection (and drop it) for getting the block NAME from the system SYSTEM.")
  (:method ((system standard-system) name)
    (let ((info (pop (gethash name (system-interesting-blocks system)))))
      (unless (nth-value 1 (gethash name (system-interesting-blocks system)))
        (remhash name (system-interesting-blocks system)))
      (if (null info)
          nil
          (block-info-connection info)))))
