(in-package :decentralise-standard-system)

(defmacro with-consistent-block ((name system) &body body)
  "Run BODY in an environment where the state of the system with respect to the block named BLOCK is internally consistent."
  (alexandria:once-only (system name)
    (alexandria:with-gensyms (lock new-lock)
      `(let (,lock)
         ;; Find the appropriate lock, if there is one.
         (decentralise-utilities:with-unlocked-box
             (table (block-consistency-locks ,system))
           (multiple-value-bind (lock-data present?)
               (gethash ,name table)
             (if present?
                 ;; If there is a lock already, wait for that.
                 (progn
                   (incf (cdr lock-data))
                   (setf ,lock (car lock-data)))
                 ;; Otherwise, make a new lock and hold that.
                 (let ((,new-lock (bt:make-lock "Block consistency lock")))
                   (setf (gethash ,name table) (cons ,new-lock 1)
                         ,lock ,new-lock)))))
         (unwind-protect
              (bt:with-lock-held (,lock)
                ,@body)
           (decentralise-utilities:with-unlocked-box
               (table (block-consistency-locks ,system))
             (symbol-macrolet ((count (cdr (gethash ,name table))))
               (decf count)
               (when (zerop count)
                 (remhash ,name table)))))))))
