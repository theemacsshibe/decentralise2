(in-package :decentralise-system)

(defclass echo-system (system)
  ((connection-states :initform (make-hash-table)
                      :reader system-connection-states)))

(defmethod leader-loop ((system echo-system))
  (loop
     (when (system-shutdown-request system)
       (return-from leader-loop))
    (sleep 0.1)))

(defmethod add-connection :after ((system echo-system) connection)
  (setf (decentralise-connection:connection-message-handler connection)
        (lambda (message)
          (decentralise-connection:write-message connection message))))
