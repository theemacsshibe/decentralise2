(in-package :decentralise-system)

(defclass memory-database-mixin ()
  ((data-table :initform (make-hash-table :test 'equal)
               :reader memory-database-data-table)))

(defmethod get-block ((system memory-database-mixin) name)
  (multiple-value-bind (value win)
      (gethash name (memory-database-data-table system))
    (if win
        (destructuring-bind (version channels data) value
          (values version channels data))
        (error 'not-found))))

(defmethod put-block ((system memory-database-mixin) name version channels data)
  (setf (gethash name (memory-database-data-table system))
        (list version channels data)))

(defmethod map-blocks (function (system memory-database-mixin))
  (maphash (lambda (name data)
             (destructuring-bind (version channels data) data
               (declare (ignore data))
               (funcall function name version channels)))
           (memory-database-data-table system)))
