(in-package :decentralise-system)

(defclass lock-mixin ()
  ()
  (:documentation "A mixin that disables locking methods that access the backend of its system."))

(defmacro call-next-locked-method (system)
  `(bt:with-lock-held ((system-backend-lock ,system))
     (call-next-method)))

(defvar *from-get-block-metadata* nil)

(defmethod put-block :around ((system lock-mixin) name version channels data)
  (call-next-locked-method system))
(defmethod get-block :around ((system lock-mixin) name)
  (if *from-get-block-metadata*
      (call-next-method)
      (call-next-locked-method system)))
(defmethod get-block-metadata :around ((system lock-mixin) name)
  (let ((*from-get-block-metadata* t))
    (call-next-locked-method system)))
(defmethod map-blocks :around (function (system lock-mixin))
  (call-next-locked-method system))
