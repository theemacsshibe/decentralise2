;;; I haven't a clue how to lay out all these components, since there
;;; are many circular parts in decentralise2. Caveat emptor.

(defpackage :decentralise-utilities
  (:use :cl)
  (:export #:defexpectation
           #:message-case
           #:make-locked-box #:with-unlocked-box #:box #:box-value
           #:with-thread #:with-semaphore
           #:doit #:dolistit
           #:handler-case*
           #:never-null-let*
           #:parse-id #:render-id
           #:map-listing #:map-nodes
           #:write-listing-line #:write-nodes-line
           #:drop-spaces #:read-word
           #:read-length-prefixed-string #:write-length-prefixed-string
           #:read-integer #:write-integer
           #:write-listing-data #:read-listing-data
           #:write-node-data #:read-node-data
           #:map-binary-listing #:map-binary-nodes
           #:with-output-to-byte-array))

(defpackage :decentralise-acceptor
  (:use :cl :decentralise-utilities)
  (:export #:start-acceptor #:stop-acceptor #:acceptor #:socket-acceptor #:acceptor-loop))

(defpackage :decentralise-connection
  (:use :cl :decentralise-utilities :split-sequence)
  (:export #:connection #:netfarm-format-connection
           #:character-connection #:binary-connection
           #:passing-connection #:make-hidden-socket
           #:make-connection-listeners #:stop-connection
           #:connection-message-handler
           #:with-errors-blamed-on-connection
           #:read-message #:write-message
           #:protocol-creator #:define-protocol
           #:connection-from-uri
           #:connection-uri #:connection-uri-protocol-name
           #:connection-created-by-me-p
           #:write-block #:request-block #:request-blocks
           #:write-ok #:write-error
           #:announce-node #:allow-announcement
           #:connection-channels #:connection-destructors
           #:connection-id
           #:connection-stopped-p #:connection-announcing-p
           #:connection-element-type
	   #:connection-counter-value #:with-connection-counter
           #:connection-known-blocks #:add-known-block #:map-known-blocks
           #:subscribe #:write-subscription))

(defpackage :decentralise-kademlia
  (:use :cl :decentralise-utilities)
  (:export #:parse-hash-name
           #:kademlia-mixin
           #:kademlia-client #:parallel-kademlia-client
           #:kademlia-system-mixin
           #:kademlia-system-integer-distance
           #:add-connection-from-uri))

(defpackage :decentralise-messages
  (:use :cl)
  (:export #:define-message-type #:message-case #:message
           #:message-accessors #:message-type-specifier #:message-constructor
           #:make-get-blocks #:get-blocks #:get-blocks-names
           #:make-put-block #:put-block #:put-block-name #:put-block-version
           #:put-block-channels #:put-block-data
           #:make-ok-response #:ok-response #:ok-response-name
           #:make-error-response #:error-response #:error-response-name
           #:error-response-reason
           #:make-new-subscriptions #:new-subscriptions
           #:new-subscriptions-subscriptions
           #:make-subscription #:subscription #:subscription-name
           #:subscription-version #:subscription-channels
           #:make-announcement-control #:announcement-control
           #:announcement-control-allow-p
           #:make-node-announcement #:node-announcement #:node-announcement-uri
           #:node-announcement-id))

(defpackage :decentralise-system
  (:use :cl :decentralise-utilities)
  (:export #:system #:echo-system
           #:put-block #:get-block #:get-block-metadata #:map-blocks
           #:new-version-p
           #:memory-database-mixin #:lock-mixin
           #:memory-database-data-table
           #:parse-hash-name
           #:start-system #:stop-system
           #:system-shutdown-request
           #:connect-system #:add-connection
           #:decentralise-error #:describe-decentralise-error
           #:not-found
           #:system-connections #:system-acceptors #:system-id
           #:leader-loop
           #:system-connections))
