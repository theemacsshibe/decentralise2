(in-package :decentralise-kademlia)

;;; Retrieving blocks is handled using a "scheduler", which can be told to
;;; run certain callbacks in a simple thread pool after some delay. This allows
;;; the client to query another subclient after one has not responsed for some
;;; time.

(defstruct event function time)

(defclass scheduler ()
  ((events :initform (decentralise-utilities:make-locked-box
                      :value (make-instance 'cl-heap:fibonacci-heap
                                            :key #'event-time))
           :reader scheduler-event-heap)
   (running :initform (box t)
            :reader %scheduler-running-p)
   (period :initarg :period :initform 0.02
           :reader scheduler-period)
   (threads :accessor scheduler-threads)))

(defgeneric next-event (scheduler)
  (:documentation "Get the next event the scheduler should handle or NIL")
  (:method ((scheduler scheduler))
    (decentralise-utilities:with-unlocked-box
        (heap (scheduler-event-heap scheduler))
      (let ((event (cl-heap:peep-at-heap heap)))
        (cond
          ((null event) nil)
          ((< (event-time event) (get-internal-real-time))
           (cl-heap:pop-heap heap))
          (t nil))))))

(defun scheduler-running-p (scheduler)
  (decentralise-utilities:with-unlocked-box
      (running (%scheduler-running-p scheduler))
    running))
(defun (setf scheduler-running-p) (running? scheduler)
  (decentralise-utilities:with-unlocked-box
      (running (%scheduler-running-p scheduler))
    (setf running running?)))

(defgeneric scheduler-loop (scheduler)
  (:method ((scheduler scheduler))
    (loop until (not (scheduler-running-p scheduler))
          do (loop for event = (next-event scheduler)
                   until (null event)
                   do (funcall (event-function event)))
             (sleep (scheduler-period scheduler)))))

(defgeneric start-scheduler (scheduler &key threads)
  (:method ((scheduler scheduler) &key (threads 1))
    (setf (scheduler-running-p scheduler) t)
    (setf (scheduler-threads scheduler)
          (loop repeat threads
                collect (decentralise-utilities:with-thread
                            (:name "Scheduler thread")
                          (scheduler-loop scheduler))))))

(defgeneric stop-scheduler (scheduler)
  (:method ((scheduler scheduler))
    (setf (scheduler-running-p scheduler) nil)))

(defun seconds-from-now (seconds)
  (+ (* seconds internal-time-units-per-second)
     (get-internal-real-time)))

(defgeneric add-event (scheduler event)
  (:method ((scheduler scheduler) event)
    (decentralise-utilities:with-unlocked-box
        (heap (scheduler-event-heap scheduler))
      (cl-heap:add-to-heap heap event))))

(defmacro do-after ((scheduler seconds) &body body)
  "Evaluate BODY after some time."
  `(add-event ,scheduler
              (make-event :time (seconds-from-now ,seconds)
                          :function (lambda () ,@body))))
(defmacro call-after (scheduler seconds thunk)
  "Call THUNK after some time."
  `(add-event ,scheduler
              (make-event :time (seconds-from-now ,seconds)
                          :function ,thunk)))
