(in-package :decentralise-kademlia)

;;; Searching for new connections using the Kademlia client is handled using
;;; a kind of dynamic thread pool, instead of the simple scheduler used for
;;; searching. This is because creating connections in decentralise2 is
;;; synchronous (to allow MAKE-INSTANCE and signalling conditions to work
;;; normally), unlike the connection messaging system.

;;; All the information required to fulfil a job is stored in a SEARCH-JOB
;;; object, including the name of the block we are searching for, a priority
;;; queue of URI information objects to look through, the callbacks we want to
;;; invoke when we succeed or fail searching, and some stuff we would need to
;;; maintain thread pools and to update the priority queue with new URI
;;; information.
(defstruct search-job
  uri-queue name distance-function
  success-continuation failure-continuation
  (done? (box nil :copier #'identity))
  (running-threads (box 0 :copier #'identity)))

(defclass searcher ()
  ((maximum-threads :initform 10
                    :initarg :maximum-threads
                    :reader searcher-maximum-threads
                    :documentation "The total number of threads this searcher can create.")
   (threads-per-search :initform 3
                       :initarg :threads-per-search
                       :reader searcher-threads-per-search
                       :documentation "The number of threads that should be delegated to one search.")
   (current-threads :initform (box 0 :copier #'identity)
                    :reader searcher-current-threads
                    :documentation "The current number of threads running.")
   (job-mailbox :initform (safe-queue:make-mailbox :name "Searcher job mailbox")
                :reader searcher-job-mailbox)
   (client :initarg :client :reader searcher-client))
  (:documentation "An object that contains all the state for searching nodes in a Kademlia world on behalf of a Kademlia client."))

(defmethod initialize-instance :after ((searcher searcher) &key)
  (assert (plusp (searcher-maximum-threads searcher)))
  (assert (plusp (searcher-threads-per-search searcher))))

(defgeneric run-search-job/uri (searcher job uri)
  (:method ((searcher searcher) (job search-job) uri)
    ;; Try to connect to this URI.
    (let ((client (searcher-client searcher)))
      (handler-case
          (trivial-timeout:with-timeout
              ((kademlia-client-subclient-timeout client))
            (decentralise-client:connect-to-uri
             (uri-information-uri uri)
             :class (kademlia-client-subclient-class client)
             :should-get-system-info t))
        (error ())
        (:no-error (subclient)
          (handler-case (add-subclient-information client uri subclient)
            (error ()
              ;; Try to dispose of this client.
              (ignore-errors (decentralise-client:stop-client client)))
            (:no-error (&rest rest)
              (declare (ignore rest))
              ;; We're done! Did we find the block we were searching for?
              (when (decentralise-client:client-has-block-p
                     subclient
                     (search-job-name job))
                ;; If we did, schedule the success continuation, remove the
                ;; job from the client's current jobs and go home.
                (with-unlocked-box (done? (search-job-done? job))
                  (setf done? t))
                (with-unlocked-box (count (search-job-running-threads job))
                  (decf count))
                (with-unlocked-box
                    (searches (%kademlia-client-current-searches client))
                  (setf searches (remove job searches)))
                (call-after (kademlia-client-scheduler client)
                            0
                            (search-job-success-continuation job)))
              t)))))))

(defgeneric run-search-job (searcher job)
  (:documentation "Run a search job, by testing each URI in the queue, and appropriately handling thread synchronisation values.") 
  (:method ((searcher searcher) (job search-job))
    (with-unlocked-box (count (search-job-running-threads job))
      (incf count))
    (loop
      ;; Get out if another thread has told us we're done. 
      (when (box-value (search-job-done? job))
        (with-unlocked-box (count (search-job-running-threads job))
          (decf count))
        (return-from run-search-job))
      (let ((uri (with-unlocked-box (queue (search-job-uri-queue job))
                   (cl-heap:dequeue queue))))
        (when (null uri)
          ;; Tell everyone we're done, remove the job from the client's current
          ;; jobs, then schedule the failure continuation.
          (with-unlocked-box (count (search-job-running-threads job))
            (decf count)
            (when (zerop count)
              (with-unlocked-box (done? (search-job-done? job))
                (setf done? t))
              (with-unlocked-box
                  (searches (%kademlia-client-current-searches
                             (searcher-client searcher)))
                (setf searches (remove job searches)))
              (call-after (kademlia-client-scheduler (searcher-client searcher))
                          0
                          (search-job-failure-continuation job))))
          (return-from run-search-job))
        (when (run-search-job/uri searcher job uri)
          (return-from run-search-job))))))

(defgeneric worker-loop (searcher)
  (:documentation "Run search jobs until no new jobs arise for some time.") 
  (:method ((searcher searcher))
    (with-unlocked-box (count (searcher-current-threads searcher))
      (incf count))
    (loop for job = (safe-queue:mailbox-receive-message
                     (searcher-job-mailbox searcher)
                     :timeout 2)
          until (null job)
          do (run-search-job searcher job))
    (with-unlocked-box (count (searcher-current-threads searcher))
      (decf count))))

(defgeneric regenerate-threads (searcher job)
  (:documentation "Regenerate the threads for a job by enqueuing the job enough times to run it with its allowed number of threads, and create more worker threads.")
  (:method ((searcher searcher) (job search-job))
    (dotimes (n (- (searcher-threads-per-search searcher)
                   (box-value (search-job-running-threads job))))
      (safe-queue:mailbox-send-message (searcher-job-mailbox searcher) job)
      (when (< (box-value (searcher-current-threads searcher))
               (searcher-maximum-threads searcher))
        (with-thread (:name "Search worker thread")
          (worker-loop searcher))))))
    
(defgeneric add-search-job (searcher job)
  (:documentation "Add the search job to the searcher.")
  (:method ((searcher searcher) (job search-job))
    (with-unlocked-box (searches (%kademlia-client-current-searches
                                  (searcher-client searcher)))
      (push job searches))
    (regenerate-threads searcher job)))

(defgeneric add-node-information-to-job (searcher job uri)
  (:documentation "Add URI information to the job's URI queue.")
  (:method ((searcher searcher) (job search-job) uri)
    (let ((distance (funcall (search-job-distance-function job)
                             (uri-information-hash uri))))
      (with-unlocked-box (queue (search-job-uri-queue job))
        (cl-heap:enqueue queue uri distance))
      (regenerate-threads searcher job))))

(defgeneric ensure-client-with-name-is-reachable (client name success fail)
  (:documentation "Ensure that the client can reach the block with given name, then call the success continuation.
If the object cannot be reached, call the failure continuation with an appropriate condition.")
  (:method :around ((client kademlia-client) name success fail)
    ;; Don't do anything if the name is reachable.
    (if (null (compute-applicable-subclients client name))
        (call-next-method)
        (funcall success)))
  (:method ((client kademlia-client) name success fail)
    (let ((hash  (parse-hash-name client name))
          (queue (make-instance 'cl-heap:priority-queue)))
      (when (null hash)
        (funcall fail (make-condition 'simple-error
                                      :format-control "Can't parse the hash name ~s"
                                      :format-arguments (list name)))
        (return-from ensure-client-with-name-is-reachable))
      (flet ((distance-function (uri-hash)
               (logxor uri-hash hash))
             (fail ()
               (funcall fail (make-condition 'decentralise-client:remote-error
                                             :name name
                                             :message "not found"))))
        (dolist (uri-info (kademlia-client-known-uris client))
          (cl-heap:enqueue queue
                           uri-info
                           (distance-function (uri-information-hash uri-info))))
        (add-search-job (kademlia-client-searcher client)
                        (make-search-job
                         :uri-queue (box queue)
                         :name name
                         :distance-function #'distance-function
                         :success-continuation success
                         :failure-continuation #'fail))))))
