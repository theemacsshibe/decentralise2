;;;; Implements some aspects of the Kademlia system
;;;; as described in http://www.scs.stanford.edu/~dm/home/papers/kpos.pdf
(in-package :decentralise-kademlia)

(defclass kademlia-mixin () ())

(defclass kademlia-system-mixin (decentralise-kademlia:kademlia-mixin)
  ((integer-distance :initarg :distance
                     :initform 256
                     :reader kademlia-system-integer-distance)))

(defgeneric parse-hash-name (kademlia name)
  (:documentation "Parse a block name containing a hash somehow, or NIL")
  (:method ((kademlia kademlia-mixin) name)
    "The default KADEMLIA-MIXIN parses a hexadecimal string"
    (when (= (length name) 64)
      (values (ignore-errors
               (parse-integer name :radix 16))))))

(defmethod decentralise-standard-system:interesting-block-p
    ((system kademlia-system-mixin) name version channels)
  (let ((hash (parse-hash-name system name)))
    (and (call-next-method)
         (or (null hash)
             (<= (integer-length (logxor hash (decentralise-system:system-id system)))
                 (kademlia-system-integer-distance system))))))
  
(decentralise-standard-system:define-special-block
    (kademlia-system-mixin "distance")
  :get (lambda (system connection)
         (declare (ignore connection))
         (values 0 '() (princ-to-string (kademlia-system-integer-distance system)))))
