(in-package :decentralise-connection)

;;; "URI" support
(defvar *protocols* (make-hash-table :test 'equalp))
(defun connection-from-uri (uri &rest initargs)
  "Create a connection to a URI (with possible initargs for some protocols)."
  (destructuring-bind (protocol host)
      (split-sequence #\: uri)
    (multiple-value-bind (constructor defined?)
        (gethash protocol *protocols*)
      (if defined?
          (apply constructor host initargs)
          (error "protocol ~a not found" protocol)))))
(defmacro define-protocol (name creator)
  "Define a URI protocol from a protocol name NAME and a function CREATOR that creates a connection of that protocol from a host name."
  `(setf (gethash ,(string-upcase name) *protocols*) ,creator))
(defun protocol-creator (name)
  "Return the function that, when called with a hostname then initargs, creates a connection of the URI protocol named."
  (values (gethash (string-upcase name) *protocols*)))

(defgeneric connection-uri (connection)
  (:documentation "Return a URI that could possibly be used to create a connection with an identical endpoint."))
