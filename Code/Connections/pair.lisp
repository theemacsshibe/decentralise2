(in-package :decentralise-connection)

(defclass passing-connection ()
  ((base-class :reader passing-connection-base-class)
   (target-connection :initarg :target :accessor passing-connection-target)
   (message-mailbox :initform (safe-queue:make-mailbox)
                    :reader passing-connection-mailbox)
   (name :initarg :name :initform "Passing-Connection" :reader passing-connection-name)
   (direction :initarg :direction :initform nil :reader passing-connection-direction)
   (worker-thread :accessor passing-connection-thread))
  (:documentation "Pass all messages sent to this connection to the TARGET. Two of these linked to each other work like a socket."))

(defun change-class-by-target-element-type (connection)
  (let ((type (if (slot-boundp connection 'target-connection)
                   (connection-element-type connection)
                   t)))
    (change-class connection (passing-connection-base-class connection))
    (alexandria:switch (type :test #'subtypep)
      ('(unsigned-byte 8)
       (dynamic-mixins:ensure-mix connection
                                  'binary-connection))
      ('character
       (dynamic-mixins:ensure-mix connection
                                  'character-connection))
      ('t
       (dynamic-mixins:ensure-mix connection
                                  'binary-connection
                                  'character-connection)))))

(defmethod (setf passing-connection-target) :after
    (new-target (connection passing-connection))
  (unless (slot-boundp connection 'base-class)
    (change-class-by-target-element-type connection)))
                                               
(defmethod print-object ((connection passing-connection) stream)
  (print-unreadable-object (connection stream :type t)
    (format stream ":Name ~s :Direction ~s"
            (passing-connection-name connection)
            (passing-connection-direction connection))))

(defmethod connection-element-type ((connection passing-connection))
  (connection-element-type (passing-connection-target connection)))

(defun passing-connection-thread-loop (connection)
  (lambda ()
    (loop until (connection-stopped-p connection)
          for message = (safe-queue:mailbox-receive-message
                         (passing-connection-mailbox connection)
                         :timeout 1)
          unless (null message)
            do (handle-message (passing-connection-target connection) message))))

(defmethod initialize-instance :after ((connection passing-connection) &key)
  ;; We will change classes to a dynamic mixin, so we should preserve the
  ;; original class.
  (setf (slot-value connection 'base-class) (class-of connection))
  (change-class-by-target-element-type connection)
  (setf (passing-connection-thread connection)
        (bt:make-thread (passing-connection-thread-loop connection)
                        :name "Passing-Connection worker thread")))

(defmethod stop-connection ((connection passing-connection))
  (stop-connection (passing-connection-target connection)))

(defun handle-message-on-passing-connection (connection message)
  (safe-queue:mailbox-send-message (passing-connection-mailbox connection)
                                   message))

(defmethod write-message ((connection passing-connection) message)
  (handle-message-on-passing-connection connection message))

(defun make-hidden-socket (&key (class 'passing-connection) (name "hidden socket") (initargs '()))
  "Return two PASSING-CONNECTIONs that work like a socket, sending messages to each other.  
If CLASS is provided, that class will be instantiated instead. 
NAME is the name of the PASSING-CONNECTIONs created.
See PASSING-CONNECTION."
  (let ((connection1 (apply #'make-instance class :name name :direction 1 initargs))
        (connection2 (apply #'make-instance class :name name :direction 2 initargs)))
    (setf (passing-connection-target connection1) connection2
          (passing-connection-target connection2) connection1)
    (values connection1 connection2)))
