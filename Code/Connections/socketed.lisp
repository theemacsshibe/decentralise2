(in-package :decentralise-connection)

(defclass socketed-connection (connection)
  ((lock   :initform (bt:make-lock "SOCKETED-CONNECTION stream lock")
           :reader connection-lock)
   (stream :initarg :stream :reader connection-stream)
   (listener :initform nil :accessor socketed-connection-listener)
   (flush-buffer-every :initarg :flush-buffer-every :initform 1
                       :reader connection-flush-buffer-every)
   (unflushed-count :initform 0 :accessor connection-unflushed-count)
   (alive :initform t :accessor connection-alive-p)
   (socket :initarg :socket :reader connection-socket)))

(defmacro with-connection-stream ((stream connection) &body body)
  (alexandria:once-only (connection)
    `(progn
       (check-type ,connection socketed-connection)
       (bt:with-lock-held ((connection-lock ,connection))
         (let ((,stream (connection-stream ,connection)))
           (declare (ignorable ,stream))
           ,@body)))))

(defmethod initialize-instance :after ((connection socketed-connection) &key)
  (setf (socketed-connection-listener connection)
        (with-thread (:name (format nil "~a input listener" (type-of connection)))
          (handler-case
              (loop for message = (read-message connection)
                    do (handle-message connection message))
            (end-of-file ()
              (stop-connection connection))
            (error (e)
              (format *debug-io* "Error on ~a:~% ~a~%" connection e)
              (stop-connection connection))))))

(defmethod stop-connection ((connection socketed-connection))
  (let ((thread (socketed-connection-listener connection)))
    (ignore-errors
      (close (connection-stream connection)))
    (unless (or (null thread) (eq thread (bt:current-thread)))
      (bt:destroy-thread thread))
    (setf (socketed-connection-listener connection) nil
          (connection-alive-p connection) nil)
    (ignore-errors
      (usocket:socket-close (connection-socket connection)))))

(defgeneric connection-uri-protocol-name (connection)
  (:documentation "Return the protocol name used for a connection URI, such as \"netfarm\"."))

(defun format-ip (ip)
  (ecase (length ip)
    (4  (usocket:vector-quad-to-dotted-quad ip))
    (16 (usocket:vector-to-ipv6-host ip))))

(defmethod connection-uri ((connection socketed-connection))
  (format nil "~a:~a"
          (connection-uri-protocol-name connection)
          (format-ip
           (usocket:get-peer-address
            (connection-socket connection)))))

(defmethod write-message :after ((connection socketed-connection) message)
  (bt:with-lock-held ((connection-lock connection))
    (when (= (incf (connection-unflushed-count connection))
             (connection-flush-buffer-every connection))
      (setf (connection-unflushed-count connection) 0)
      (finish-output (connection-stream connection)))))
