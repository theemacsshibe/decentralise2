(in-package :decentralise-acceptor)

(defclass socket-acceptor (acceptor)
  ((server-socket :accessor server-socket)
   (host :initform "0.0.0.0" :initarg :host :reader acceptor-host)
   (port :initform 1892 :initarg :port :reader acceptor-port)
   (certificate :initform nil :initarg :certificate :reader acceptor-certificate)
   (key :initform nil :initarg :key :reader acceptor-key)
   (connection-class :initarg :connection-class
                     :initform (alexandria:required-argument 'connection-class)
                     :reader connection-class)))

(defmethod start-acceptor :before ((acceptor socket-acceptor) system)
  (setf (server-socket acceptor)
        (usocket:socket-listen (acceptor-host acceptor)
                               (acceptor-port acceptor)
                               :reuse-address t)))

(defmethod stop-acceptor :before ((acceptor socket-acceptor))
  "Close the listening socket when we're done with it"
  (ignore-errors
    (usocket:socket-close (server-socket acceptor))))

(defmethod accept-connection ((acceptor socket-acceptor))
  (let* ((socket (usocket:socket-accept (server-socket acceptor)))
         (name (usocket:get-peer-name socket))
         (port (usocket:get-peer-port socket))
         (stream (cl+ssl:make-ssl-server-stream (usocket:socket-stream socket)
                                                :external-format :utf-8
                                                :certificate (acceptor-certificate acceptor)
                                                :key (acceptor-key acceptor)))
         (connection (make-instance (connection-class acceptor)
                                    :socket socket :stream stream)))
    (format *debug-io* "~a:~d connected~%" name port)
    (push (lambda (connection)
            (declare (ignore connection))
            (format *debug-io* "~a:~d disconnected~%" name port))
          (decentralise-connection:connection-destructors connection))
    connection))
