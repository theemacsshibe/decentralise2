(in-package :decentralise-acceptor)

(defclass acceptor ()
  ((system :accessor system)
   (connector-thread :accessor connector-thread)))

(defgeneric start-acceptor (acceptor system)
  (:method ((acceptor acceptor) system)
    (setf (system acceptor) system
          (connector-thread acceptor)
          (with-thread (:name (format nil "decentralise2 ~a" (type-of acceptor)))
            (acceptor-loop acceptor)))))

(defgeneric acceptor-loop (acceptor)
  (:method ((acceptor acceptor))
    (loop
      (handler-case
          (let ((connection (accept-connection acceptor)))
            (decentralise-system:add-connection (system acceptor) connection))
        (error (e)
          (format *debug-io* "*** ~a~%" e))))))

(defgeneric accept-connection (acceptor))

(defgeneric stop-acceptor (acceptor)
  (:method ((acceptor acceptor))
    (handler-case
        (bt:destroy-thread (connector-thread acceptor))
      (error (e)
        (declare (ignore e))
        (error "Acceptor is already stopped")))))
