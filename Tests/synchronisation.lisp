(in-package :decentralise-tests)

(def-suite synchronisation)
(in-suite synchronisation)

;;; To my knowledge, sb-sprof does not work on SBCL on Windows. 
#+(and :sbcl (not :windows) (not :gitlab-ci))
(eval-when (:compile-toplevel :load-toplevel :execute)
  (require 'sb-sprof)
  ;; Force PRINT-OBJECT's discriminator function to be compiled.
  ;; This should avoid a nasty deadlock between the profiler and the world lock
  ;; that is held while discriminator functions are compiled.
  (print-object (list) (make-broadcast-stream)))

(defun random-string (&key (length 16) (bag "1234567890abcdefghijklmnopqrstuvwxyz"))
  (let ((string (make-string length)))
    (dotimes (position length)
      (setf (char string position)
            (char bag (random (length bag)))))
    string))

(defvar *table-header* "    time   blocks  threads requests")
(defun print-statistics (time system)
  (format *debug-io* "~&~8,2f ~8d ~8d ~8d~%"
          time
          (hash-table-count (memory-database-data-table system))
          (length (bt:all-threads))
          (hash-table-count
           (decentralise-standard-system::scheduler-current-requests
            system))))

(defun watch-systems (system1 system2)
  (loop with size1 = (hash-table-count
                      (memory-database-data-table system1))
        with start-time = (get-internal-real-time)
        for time = (/ (- (get-internal-real-time) start-time)
                      internal-time-units-per-second)
        for size2 = (hash-table-count
                     (memory-database-data-table system2))
        for print-row? = (<= (mod time 1/2) 1/100)
        until (> time 20)
        if (= size2 size1)
          do (format *debug-io* "~&Everything synchronised in ~$ second~:p!~%"
                     time)
             (return t)
        else do (when print-row?
                  (print-statistics time system2))
                (sleep 0.01)
        finally (return nil)))

(def-test synchronises-everything ()
    (let ((system1 (make-instance 'test-system :timeout 10))
          (system2 (make-instance 'test-system :timeout 10 :concurrent-requests 80)))
      (start-system system1)
      (start-system system2)
      ;; Populate system1's data table.
      (dotimes (n 100000)
        (let ((name (random-string))
              (data (random-string :length 128)))
          (setf (gethash name (memory-database-data-table system1))
                (list 1 '() data))))
      #+(and :sbcl (not :windows) (not :gitlab-ci))
      (sb-sprof:start-profiling :sample-interval 0.001)
      (multiple-value-bind (connection1 connection2)
          (attach-systems system1 system2)
        (declare (ignore connection1))
        (format *debug-io* "~&Synchronising ~d item~:p~%"
                (hash-table-count
                 (decentralise-system:memory-database-data-table system1)))
        (request-block connection2 "list")
        (unwind-protect
             (progn
               (format *debug-io* "~&Waiting for synchronisation to start...~%")
               (loop for time from 0.0 to 20.0 by 0.01
                     if (zerop (hash-table-count
                                (memory-database-data-table system2)))
                       do (sleep 0.01)
                     else
                       do (format *debug-io* "~&Synchronisation started after ~$ second~:p~%" time)
                          (return))
               (write-line *table-header* *debug-io*)
               (is-true (watch-systems system1 system2)
                        "The synchronisation test failed."))
          #+(and :sbcl (not :windows) (not :gitlab-ci)) 
          (progn (sb-sprof:stop-profiling)
                 (sb-sprof:report :type :flat :min-percent 0.5))
          (stop-system system1)
          (stop-system system2)))))
                   
