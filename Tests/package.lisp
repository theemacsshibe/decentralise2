(defpackage decentralise-tests
  (:use :cl
        :decentralise-system :decentralise-standard-system
        :decentralise-acceptor :decentralise-connection
        :decentralise-utilities
        :fiveam)
  (:export #:run-tests #:gitlab-ci-test))
(in-package :decentralise-tests)

(defun run-tests ()
  (run! '(synchronisation client-tests kademlia-client)))

(defun gitlab-ci-test ()
  (or
   (run-tests)
   (uiop:quit -1)))
